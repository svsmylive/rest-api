<?php

namespace App\Console\Commands;

use App\Domain\Posts\Models\Post;
use App\Domain\Ratings\Actions\CancelVotesAction;
use App\Domain\UserRatings\Actions\RemoveUserVotesAction;
use Illuminate\Console\Command;
use Symfony\Component\VarDumper\VarDumper;

class ResetPostRatingCommand extends Command
{
    public function __construct(private readonly CancelVotesAction $cancelVotesAction, private readonly RemoveUserVotesAction $removeUserVotesAction)
    {
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:reset-rating {post}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $post = Post::find($this->argument('post'));

        if (!$post) {
            return VarDumper::dump(Command::INVALID);
        }

        \DB::transaction(function () use ($post) {
            $this->removeUserVotesAction->execute($post->id);
            $this->cancelVotesAction->execute($post->rating);
        });

        return VarDumper::dump(Command::SUCCESS);
    }
}
