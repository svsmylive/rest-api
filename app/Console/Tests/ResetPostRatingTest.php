<?php

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);

test("Command orders:order-pay {orderId} success", function () {
    /** @var ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withPaymentStart()->create();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    artisan("orders:order-pay {$order->id}");

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'payment_status' => PaymentStatusEnum::PAID,
    ]);
});
