<?php

namespace App\Http\ApiV1\Requests\Post;

use App\Domain\Posts\Models\Tag;
use App\Domain\Posts\Models\Topic;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'user_id' => ['required', 'integer'],
            'preview_image' => ['nullable', 'string'],
            'preview_text' => ['nullable', 'string'],
            'detail_text' => ['nullable', 'string'],
            'tags' => ['nullable', 'array'],
            'tags.*' => ['required', Rule::exists(Tag::class, 'id')],
            'topics' => ['nullable', 'array'],
            'topics.*' => ['required', Rule::exists(Topic::class, 'id')],
        ];
    }
}
