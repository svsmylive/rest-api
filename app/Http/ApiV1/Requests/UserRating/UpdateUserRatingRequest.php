<?php

namespace App\Http\ApiV1\Requests\UserRating;

use App\Domain\Posts\Models\Post;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'user_id' => ['required', 'int'],
            'post_id' => ['required', Rule::exists(Post::class, 'id')],
            'rating' => ['required', 'numeric', 'between:-1,1'],
        ];
    }
}
