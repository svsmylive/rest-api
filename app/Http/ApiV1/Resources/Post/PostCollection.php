<?php

namespace App\Http\ApiV1\Resources\Post;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class PostCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'data' => $this->transformCollection($this->collectResource($this->collection)),
        ];
    }

    protected function transformCollection(Collection $posts): Collection
    {
        return $posts->transform(function (PostResource $post) {
            return $this->formatPostData($post);
        });
    }

    protected function formatPostData(PostResource $post): array
    {
        return [
            'id' => $post->id,
            'title' => $post->title,
            'user_id' => $post->user_id,
            'preview_image' => $post->preview_image,
            'preview_text' => $post->preview_text,
            'bookmarks' => $post->bookmarks,
            'views' => $post->views,
            'rating' => $post->rating,
            'tags' => $post->whenLoaded('tags'),
            'topics' => $post->whenLoaded('topics'),
        ];
    }
}
