<?php

namespace App\Http\ApiV1\Resources\Post;

use App\Domain\Ratings\Models\Rating;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'preview_image' => $this->preview_image,
            'preview_text' => $this->preview_text,
            'detail_text' => $this->detail_text,
            'rating' => $this->formatRating($this->rating),
        ];
    }

    private function formatRating(?Rating $rating): ?Rating
    {
        if (!$rating) {
            return null;
        }

        $rating->setVisible(['positive', 'negative', 'total']);

        return $rating;
    }
}
