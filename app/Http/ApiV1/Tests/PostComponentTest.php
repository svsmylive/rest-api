<?php


use App\Domain\Posts\Models\Post;
use App\Domain\Posts\Models\Tests\Factories\PostRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/posts 201', function () {
    /** @var Post $post */
    $post = Post::factory()->create();
    $postData = PostRequestFactory::new()->make();

    postJson('/api/v1/posts', $postData)
        ->assertStatus(201);
});

test('GET /api/v1/posts/{id} 200', function () {
    /** @var Post $post */
    $post = Post::factory()->create();

    getJson("/api/v1/posts/{$post->id}")
        ->assertStatus(200);
});

test('PATCH /api/v1/posts/{id} 200', function () {
    /** @var Post $post */
    $post = Post::factory()->create();
    $postData = PostRequestFactory::new()->make();

    patchJson("/api/v1/posts/{$post->id}", ['title' => $postData['title']])
        ->assertStatus(200);
});

test('POST /api/v1/posts/search:search 200', function () {
    /** @var Post $post */
    $post = Post::factory()->create();
    $title = 'title';

    postJson('/api/v1/posts/search:search', [
        'title' => $title,
        'sortBy' => '-id',
    ])
        ->assertStatus(200);
});
