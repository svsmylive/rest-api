<?php

use App\Domain\Posts\Models\Tests\Factories\PostFactory;
use App\Domain\UserRatings\Models\Tests\Factories\UserRatingFactory;
use App\Domain\UserRatings\Models\Tests\Factories\UserRatingRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\patch;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST api/v1/ratings 201', function () {
    $post = PostFactory::new()->create();
    $userRatingData = UserRatingRequestFactory::new()->make(['post_id' => $post->id]);

    postJson('api/v1/ratings', $userRatingData)
        ->assertStatus(201);
});

test('PATCH api/v1/ratings/{id} 200', function () {
    $userRating = UserRatingFactory::new()->create();
    $userRatingData = UserRatingRequestFactory::new()->make();

    patch("api/v1/ratings/{$userRating->id}", ['rating' => $userRatingData->rating])
        ->assertStatus(200);
});

test('POST api/v1/ratings/search:search 200', function () {
    $searchUser = 1;

    postJson('api/v1/ratings/search:search', [
        'searchUser' => $searchUser,
    ])
        ->assertStatus(200);
});
