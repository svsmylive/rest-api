<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Posts\Actions\CreatePostAction;
use App\Domain\Posts\Actions\DeletePostAction;
use App\Domain\Posts\Actions\PatchPostAction;
use App\Domain\Posts\Models\Post;
use App\Http\ApiV1\Filters\PostsFilter;
use App\Http\ApiV1\Requests\Post\CreatePostRequest;
use App\Http\ApiV1\Requests\Post\UpdatePostRequest;
use App\Http\ApiV1\Resources\Post\PostCollection;
use App\Http\ApiV1\Resources\Post\PostResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Routing\Controller;

class PostController extends Controller
{
    public function create(CreatePostRequest $request, CreatePostAction $action)
    {
        return new PostResource($action->execute($request->validated()));
    }

    public function show(int $postId)
    {
        return new PostResource(Post::findOrFail($postId));
    }

    public function patch(int $postId, UpdatePostRequest $request, PatchPostAction $action)
    {
        return new PostResource($action->execute($postId, $request->validated()));
    }

    public function delete(int $postId, DeletePostAction $action)
    {
        $action->execute($postId);

        return new EmptyResource();
    }

    public function search(PostsFilter $filter)
    {
        return new PostCollection(Post::filter($filter)->paginate(Post::PAGINATE_LIMIT));
    }
}
