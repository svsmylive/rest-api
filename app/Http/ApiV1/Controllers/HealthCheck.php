<?php

namespace App\Http\ApiV1\Controllers;

class HealthCheck
{
    public function __invoke()
    {
        return 'OK';
    }
}
