<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\UserRatings\Actions\CreateUserRatingAction;
use App\Domain\UserRatings\Actions\DeleteUserRatingAction;
use App\Domain\UserRatings\Actions\PatchUserRationAction;
use App\Domain\UserRatings\Models\UserRating;
use App\Http\ApiV1\Filters\RatingsFilter;
use App\Http\ApiV1\Requests\UserRating\CreateUserRatingRequest;
use App\Http\ApiV1\Requests\UserRating\UpdateUserRatingRequest;
use App\Http\ApiV1\Resources\UserRating\UserRatingCollection;
use App\Http\ApiV1\Resources\UserRating\UserRatingResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class RatingController
{
    public function create(CreateUserRatingRequest $request, CreateUserRatingAction $action)
    {
        return new UserRatingResource($action->execute($request->validated()));
    }

    public function patch(int $userRatingId, UpdateUserRatingRequest $request, PatchUserRationAction $action)
    {
        return new UserRatingResource($action->execute($userRatingId, $request->validated()));
    }

    public function delete(int $userRatingId, DeleteUserRatingAction $action)
    {
        $action->execute($userRatingId);

        return new EmptyResource();
    }

    public function search(RatingsFilter $filter)
    {
        return new UserRatingCollection(UserRating::query()
            ->filter($filter)
            ->select('id', 'user_id', 'post_id', 'rating')
            ->groupBy('post_id')
            ->paginate(UserRating::PAGINATE_LIMIT));
    }
}
