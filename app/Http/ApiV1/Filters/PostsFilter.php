<?php

namespace App\Http\ApiV1\Filters;

use Illuminate\Database\Eloquent\Builder;

class PostsFilter extends QueryFilter
{
    private array $sortColumns = [
        'id', 'title', 'bookmarks', 'views',
        'rating', 'created_at', 'updated_at', 'sortVehicle',
    ];

    private array $relations = [
        'topics', 'tags',
    ];

    public function title(array|string $value)
    {
        if (is_string($value)) {
            $this->builder->where('title', 'like', '%' . $value . '%');
        }

        if (is_array($value)) {
            foreach ($value as $title) {
                $this->builder->where(function (Builder $query) use ($title) {
                    $query->where('title', 'like', '%' . $title . '%');
                });
            }
        }
    }

    public function bookmarks(array|int $bookmarks)
    {
        if (is_int($bookmarks)) {
            $this->builder->where('bookmarks', $bookmarks);
        }

        if (is_array($bookmarks)) {
            foreach ($bookmarks as $bookmark) {
                $this->builder->where(function (Builder $query) use ($bookmark) {
                    $query->where('bookmarks', $bookmark);
                });
            }
        }
    }

    public function views(array|int $views)
    {
        if (is_int($views)) {
            $this->builder->where('bookmarks', $views);
        }

        if (is_array($views)) {
            foreach ($views as $value) {
                $this->builder->where(function (Builder $query) use ($value) {
                    $query->where('bookmarks', $value);
                });
            }
        }
    }

    public function rating(array|int $rating)
    {
        if (is_int($rating)) {
            $this->builder->whereHas('rating', function (Builder $query) use ($rating) {
                $query->where('total', '>=', $rating);
            });
        }

        if (is_array($rating)) {
            foreach ($rating as $value) {
                $this->builder->whereHas('rating', function (Builder $query) use ($value) {
                    $query->where('total', '>=', $value);
                });
            }
        }
    }

    public function sortBy(array|string $value)
    {
        if (is_string($value)) {
            if (in_array(trim($value, '-'), $this->sortColumns)) {
                $value[0] !== '-' ? $this->builder->orderBy($value) : $this->builder->orderBy(trim($value, '-'), 'desc');
            }
        }

        if (is_array($value)) {
            foreach ($value as $sortValue) {
                if (in_array(trim($sortValue, '-'), $this->sortColumns)) {
                    $sortValue[0] !== '-' ? $this->builder->orderBy($sortValue) : $this->builder->orderBy(trim($sortValue, '-'), 'desc');
                }
            }
        }
    }

    public function include(array|string $value)
    {
        if (is_string($value)) {
            if (in_array($value, $this->relations)) {
                $this->builder->with($value);
            }
        }

        if (is_array($value)) {
            foreach ($value as $relation) {
                if (in_array($relation, $this->relations)) {
                    $this->builder->with($relation);
                }
            }
        }
    }
}
