<?php

namespace App\Http\ApiV1\Filters;

class RatingsFilter extends QueryFilter
{
    public function searchUser(array|int $value)
    {
        if (is_int($value)) {
            $this->builder->where('user_id', $value);
        }

        if (is_array($value)) {
            foreach ($value as $id) {
                $this->builder->orWhere('user_id', $id);
            }
        }
    }

    public function searchPost(array|int $value)
    {
        if (is_int($value)) {
            $this->builder->where('post_id', $value);
        }

        if (is_array($value)) {
            foreach ($value as $id) {
                $this->builder->orWhere('post_id', $id);
            }
        }
    }
}
