<?php

namespace App\Http\ApiV1\Filters;

use App\Http\ApiV1\Filters\Interfaces\FilterInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Http\Request;

abstract class QueryFilter implements FilterInterface
{
    protected Builder|QueryBuilder $builder;

    protected array $filters;

    public function __construct(protected Request $request)
    {
    }

    public function apply(Builder $builder): Builder|QueryBuilder
    {
        $this->builder = $builder;

        foreach ($this->filters() as $filter => $value) {
            $filter = str_replace('_', '', $filter);

            if (method_exists($this, $filter) && !is_null($value)) {
                $this->$filter($value);
            }
        }

        return $this->builder;
    }

    public function filters(): array
    {
        return $this->filters ?? $this->request->all();
    }

    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }

    public function addFilters(array $filters): void
    {
        $this->filters = array_merge($this->filters(), $filters);
    }

    public function existsFilter(string $filter): bool
    {
        return array_key_exists($filter, $this->filters());
    }
}
