<?php

namespace App\Http\ApiV1\Filters\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;

interface FilterInterface
{
    public function apply(Builder $builder): Builder|QueryBuilder;

    public function setFilters(array $filters): void;
}
