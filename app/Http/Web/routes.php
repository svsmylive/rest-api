<?php

use App\Http\ApiV1\Controllers\HealthCheck;
use App\Http\ApiV1\Controllers\SwaggerController;
use Illuminate\Support\Facades\Route;

Route::get('health', HealthCheck::class);

Route::get('/', [SwaggerController::class, 'listSwaggers']);
