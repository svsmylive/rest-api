<?php

namespace App\Domain\Ratings\Actions;

use App\Domain\Ratings\Models\Rating;

class CreateRatingAction
{
    public function execute(array $fields)
    {
        $negative = ($fields['rating'] < 0) ? 1 : 0;
        $positive = ($fields['rating'] > 0) ? 1 : 0;
        $total = $positive - $negative;
        $postId = $fields['post_id'];

        Rating::create([
            'post_id' => $postId,
            'negative' => $negative,
            'positive' => $positive,
            'total' => $total,
        ]);
    }
}
