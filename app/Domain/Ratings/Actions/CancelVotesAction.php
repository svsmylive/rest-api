<?php

namespace App\Domain\Ratings\Actions;

use App\Domain\Ratings\Models\Rating;

class CancelVotesAction
{
    public function execute(Rating $rating)
    {
        $rating->update([
            'positive' => 0,
            'negative' => 0,
            'total' => 0,
        ]);
    }
}
