<?php

namespace App\Domain\Ratings\Actions;

use App\Domain\Ratings\Models\Rating;
use App\Domain\UserRatings\Models\UserRating;
use Illuminate\Support\Arr;

class PatchRatingAction
{
    public function execute(int $postId)
    {
        $rating = Rating::filterHasPostRating($postId)->first();
        $negative = UserRating::filterNegativeVotes($postId);
        $positive = UserRating::filterPositiveVotes($postId);
        $total = $positive - $negative;

        $rating->update(Arr::only([$negative, $positive, $total], Rating::FILLABLE));
    }
}
