<?php

namespace App\Domain\Ratings\Models;

use App\Domain\Posts\Models\Post;
use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Rating extends Model
{
    use Filterable;

    public const FILLABLE = [
        'positive',
        'negative',
        'post_id',
        'total',
        'created_at',
        'updated_at',
    ];

    protected $fillable = self::FILLABLE;

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    public function scopeFilterHasPostRating(Builder $query, int $postId): Builder
    {
        return $query->where(function (Builder $query) use ($postId) {
            $query->where('post_id', $postId);
        });
    }
}
