<?php

namespace App\Domain\Posts\Models;

use Illuminate\Database\Eloquent\Model;

class PostTopic extends Model
{
    protected $fillable = [
        'post_id', 'topic_id',
        'created_at', 'updated_at',
    ];
}
