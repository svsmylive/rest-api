<?php

namespace App\Domain\Posts\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name', 'created_at', 'updated_at',
    ];
}
