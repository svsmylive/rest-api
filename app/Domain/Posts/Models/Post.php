<?php

namespace App\Domain\Posts\Models;

use App\Domain\Posts\Models\Tests\Factories\PostFactory;
use App\Domain\Ratings\Models\Rating;
use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Post extends Model
{
    use Filterable;

    public const PAGINATE_LIMIT = 15;

    public const FILLABLE = [
        'title', 'preview_image', 'preview_text', 'bookmarks', 'views', 'user_id', 'detail_text', 'created_at', 'updated_at',
    ];

    protected $fillable = self::FILLABLE;

    protected $with = ['rating'];

    public static function factory(): PostFactory
    {
        return PostFactory::new();
    }

    public function rating(): hasOne
    {
        return $this->hasOne(Rating::class);
    }

    public function topics(): BelongsToMany
    {
        return $this->belongsToMany(Topic::class, 'post_topic', 'post_id', 'topic_id');
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'post_tag', 'post_id', 'tag_id');
    }

    public function comments(): hasMany
    {
        return $this->hasMany(Comment::class);
    }
}
