<?php

namespace App\Domain\Posts\Models;

use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    protected $fillable = [
        'post_id', 'tag_id',
        'created_at', 'updated_at',
    ];
}
