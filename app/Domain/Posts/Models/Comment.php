<?php

namespace App\Domain\Posts\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    protected $fillable = [
        'comment', 'post_id',
        'created_at', 'updated_at',
    ];

    public function post(): belongsTo
    {
        return $this->belongsTo(Post::class);
    }
}
