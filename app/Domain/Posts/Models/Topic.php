<?php

namespace App\Domain\Posts\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = [
        'name', 'slug',
        'created_at', 'updated_at',
    ];
}
