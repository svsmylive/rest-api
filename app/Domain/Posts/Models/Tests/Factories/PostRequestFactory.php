<?php

namespace App\Domain\Posts\Models\Tests\Factories;

use App\Domain\Posts\Models\Post;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class PostRequestFactory extends BaseApiFactory
{
    protected $model = Post::class;

    public function definition(): array
    {
        return [
            'title' => $this->faker->title,
            'user_id' => $this->faker->unique()->randomNumber(),
            'preview_text' => $this->faker->text(50),
            'detail_text' => $this->faker->text(100),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
