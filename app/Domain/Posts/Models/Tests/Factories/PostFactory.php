<?php

namespace App\Domain\Posts\Models\Tests\Factories;

use App\Domain\Posts\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    protected $model = Post::class;

    public function definition()
    {
        return [
            'title' => $this->faker->title,
            'user_id' => $this->faker->unique()->randomNumber(),
            'preview_text' => $this->faker->text(50),
            'detail_text' => $this->faker->text(100),
        ];
    }
}
