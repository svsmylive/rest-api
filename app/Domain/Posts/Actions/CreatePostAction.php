<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;
use Illuminate\Support\Arr;

class CreatePostAction
{
    public function execute(array $fields): Post
    {
        $tags = $fields['tags'] ?? [];
        $topics = $fields['topics'] ?? [];

        /** @var Post $post */
        $post = Post::create(Arr::only($fields, Post::FILLABLE));

        $post->topics()->sync($topics);
        $post->tags()->sync($tags);

        return $post;
    }
}
