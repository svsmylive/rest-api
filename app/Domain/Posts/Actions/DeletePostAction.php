<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;

class DeletePostAction
{
    public function execute(int $postId)
    {
        Post::destroy($postId);
    }
}
