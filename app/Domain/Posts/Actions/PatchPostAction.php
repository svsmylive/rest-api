<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;
use Illuminate\Support\Arr;

class PatchPostAction
{
    public function execute(int $postId, array $fields): Post
    {
        $tags = $fields['tags'] ?? [];
        $topics = $fields['topics'] ?? [];

        $post = Post::findOrFail($postId);
        $post->update(Arr::only($fields, Post::FILLABLE));

        $post->topics()->sync($topics);
        $post->tags()->sync($tags);

        return $post;
    }
}
