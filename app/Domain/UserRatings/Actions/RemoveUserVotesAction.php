<?php

namespace App\Domain\UserRatings\Actions;

use App\Domain\UserRatings\Models\UserRating;

class RemoveUserVotesAction
{
    public function execute(int $postId)
    {
        UserRating::query()
            ->filterByPostId($postId)
            ->delete();
    }
}
