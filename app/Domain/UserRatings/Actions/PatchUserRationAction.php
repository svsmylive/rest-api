<?php

namespace App\Domain\UserRatings\Actions;

use App\Domain\Ratings\Actions\PatchRatingAction;
use App\Domain\UserRatings\Models\UserRating;
use Illuminate\Support\Arr;

class PatchUserRationAction
{
    public function __construct(private readonly PatchRatingAction $patchRatingAction)
    {
    }

    public function execute(int $id, array $fields): UserRating
    {
        $userRating = UserRating::findOrFail($id);

        $userRating = \DB::transaction(function () use ($userRating, $fields) {
            $userRating = $userRating->update(Arr::only($fields, UserRating::FILLABLE));
            $this->patchRatingAction->execute($fields['id']);

            return $userRating;
        });

        return $userRating;
    }
}
