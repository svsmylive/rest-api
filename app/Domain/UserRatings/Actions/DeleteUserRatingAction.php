<?php

namespace App\Domain\UserRatings\Actions;

use App\Domain\Ratings\Actions\PatchRatingAction;
use App\Domain\UserRatings\Models\UserRating;

class DeleteUserRatingAction
{
    public function __construct(private readonly PatchRatingAction $patchRatingAction)
    {
    }

    public function execute(int $id): void
    {
        $userRating = UserRating::findOrFail($id);

        \DB::transaction(function () use ($userRating) {
            $postId = $userRating->post_id;
            $userRating->delete();
            $this->patchRatingAction->execute($postId);
        });
    }
}
