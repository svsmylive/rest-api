<?php

namespace App\Domain\UserRatings\Actions;

use App\Domain\Ratings\Actions\CreateRatingAction;
use App\Domain\Ratings\Actions\PatchRatingAction;
use App\Domain\Ratings\Models\Rating;
use App\Domain\UserRatings\Models\UserRating;
use Illuminate\Support\Arr;

class CreateUserRatingAction
{
    public function __construct(private readonly CreateRatingAction $createRatingAction, private readonly PatchRatingAction $patchRatingAction)
    {
    }

    public function execute(array $fields): UserRating
    {
        \DB::transaction(function () use ($fields) {
            if (!UserRating::filterUserVotes($fields)->exists()) {
                UserRating::create(Arr::only($fields, UserRating::FILLABLE));
            }

            if (!Rating::filterHasPostRating($fields['post_id'])->exists()) {
                $this->createRatingAction->execute($fields);
            }

            $this->patchRatingAction->execute($fields['post_id']);
        });

        return UserRating::query()->filterUserVotes($fields)->first();
    }
}
