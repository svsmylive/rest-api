<?php

namespace App\Domain\UserRatings\Models\Tests\Factories;

use App\Domain\UserRatings\Models\UserRating;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class UserRatingRequestFactory extends BaseApiFactory
{
    protected $model = UserRating::class;

    protected function definition(): array
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 100),
            'post_id' => $this->faker->numberBetween(1, 100),
            'rating' => $this->faker->numberBetween(-1, 1),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
