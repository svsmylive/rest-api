<?php

namespace App\Domain\UserRatings\Models\Tests\Factories;

use App\Domain\UserRatings\Models\UserRating;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserRatingFactory extends Factory
{
    protected $model = UserRating::class;

    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 100),
            'post_id' => $this->faker->numberBetween(1, 100),
            'rating' => $this->faker->numberBetween(-1, 1),
        ];
    }
}
