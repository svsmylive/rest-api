<?php

namespace App\Domain\UserRatings\Models;

use App\Domain\UserRatings\Models\Tests\Factories\UserRatingFactory;
use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class UserRating extends Model
{
    use Filterable;

    public const PAGINATE_LIMIT = 15;

    public const FILLABLE = [
        'user_id', 'post_id', 'rating',
    ];

    protected $table = 'user_rating';

    protected $fillable = self::FILLABLE;

    public function scopeFilterUserVotes(Builder $query, array $data): Builder
    {
        return $query->where(function (Builder $query) use ($data) {
            $query->where('post_id', $data['post_id']);
            $query->where('user_id', $data['user_id']);
        });
    }

    public function scopeFilterPositiveVotes(Builder $query, int $postId): int
    {
        return $query->where(function (Builder $query) use ($postId) {
            $query->where('post_id', $postId);
            $query->where('rating', '>', 0);
        })->count();
    }

    public function scopeFilterNegativeVotes(Builder $query, int $postId): int
    {
        return $query->where(function (Builder $query) use ($postId) {
            $query->where('post_id', $postId);
            $query->where('rating', '<', 0);
        })->count();
    }

    public function scopeFilterByPostId(Builder $query, int $postId): Builder
    {
        return $query->where('post_id', $postId);
    }

    public static function factory(): UserRatingFactory
    {
        return UserRatingFactory::new();
    }
}
